from rest_framework import generics 
from articles.models import Article
from users.models import CustomUser
from .serializers import CustomUserSerializer
from .serializers import ArticleSerializer

class ListArticle(generics.ListCreateAPIView):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer 

class DetailArticle(generics.RetrieveUpdateDestroyAPIView):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer  

class ListUsers(generics.ListCreateAPIView):
    queryset = Article.objects.all()
    serializer_class = CustomUserSerializer 

class DetailUsers(generics.RetrieveUpdateDestroyAPIView):
    queryset = Article.objects.all()
    serializer_class = CustomUserSerializer      